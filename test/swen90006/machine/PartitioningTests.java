package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.
  @Test public void aTest()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    final int expected = 2;
    final int actual = 1 + 1;
    assertEquals(expected, actual);
  }

  @Test public void anotherTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("a");
    list.add("b");

    //the assertTrue method is used to check whether something holds.
    assertTrue(list.contains("a"));
  }

  //Test test opens a file and executes the machine
  @Test public void aFileOpenTest()
  {
    final List<String> lines = readInstructions("examples/array.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 45);
  }
  
  @Test public void EC1()
  {
    final List<String> lines = readInstructions("examples/EC1.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 1);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC2()
  {
    final List<String> lines = readInstructions("examples/EC2.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test public void EC31()
  {
    final List<String> lines = readInstructions("examples/EC3-1.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 3);
  }
  
  @Test public void EC32()
  {
    final List<String> lines = readInstructions("examples/EC3-2.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 3);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC4()
  {
    final List<String> lines = readInstructions("examples/EC4.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC5()
  {
    final List<String> lines = readInstructions("examples/EC5.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC6()
  {
    final List<String> lines = readInstructions("examples/EC6.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test public void EC71()
  {
    final List<String> lines = readInstructions("examples/EC7-1.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), -65533);
  }
  
  @Test public void EC72()
  {
    final List<String> lines = readInstructions("examples/EC7-2.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 65537);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC8()
  {
    final List<String> lines = readInstructions("examples/EC8.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC9()
  {
    final List<String> lines = readInstructions("examples/EC9.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC10()
  {
    final List<String> lines = readInstructions("examples/EC10.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test public void EC11()
  {
    final List<String> lines = readInstructions("examples/EC11.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  
  @Test public void EC121()
  {
    final List<String> lines = readInstructions("examples/EC12-1.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 3);
  }
  
  @Test public void EC122()
  {
    final List<String> lines = readInstructions("examples/EC12-2.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 3);
  }
  
  @Test public void EC13()
  {
    final List<String> lines = readInstructions("examples/EC13.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC14()
  {
    final List<String> lines = readInstructions("examples/EC14.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC15()
  {
    final List<String> lines = readInstructions("examples/EC15.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void EC16()
  {
    final List<String> lines = readInstructions("examples/EC16.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test public void EC171()
  {
    final List<String> lines = readInstructions("examples/EC17-1.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 3);
  }
  
  @Test public void EC172()
  {
    final List<String> lines = readInstructions("examples/EC17-2.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 5);
  }
  
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void EC18()
  {
    final List<String> lines = readInstructions("examples/EC18.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC19()
  {
    final List<String> lines = readInstructions("examples/EC19.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void EC20()
  {
    final List<String> lines = readInstructions("examples/EC20.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void EC21()
  {
    final List<String> lines = readInstructions("examples/EC21.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  
  @Test public void EC22()
  {
    final List<String> lines = readInstructions("examples/EC22.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  //To test an exception, specify the expected exception after the @Test
  /*@Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }*/

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
  /*@Test public void aFailedTest()
  {
    //include a message for better feedback
    final int expected = 2;
    final int actual = 1 + 2;
    assertEquals("Some failure message", expected, actual);
  }*/

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
